package server;

// import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    public static void main(String[] args) {
        final int PORT = 12345;
        ServerSocket serverSocket;
        Socket clientSocket = null;
        Scanner input = null;
        // PrintStream output = null;

        // criar o socket e fazer o bind
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (Exception e) {
            System.out.println("Porta " + PORT + " já está em uso.");
            return;
        }

        // aguardar um pedido de conexão
        try {
            System.out.println("Aguardando pedido de conexão...");
            clientSocket = serverSocket.accept();
            System.out.println("Conectado com " + clientSocket.getInetAddress().getHostAddress());
        } catch (Exception e) {
            System.out.println("Erro na conexão.");
            System.out.println(e.getMessage());
        }

        // fase de comunicação
        try {
            input = new Scanner(clientSocket.getInputStream());
            // output = new PrintStream(clientSocket.getOutputStream());

            String msg;
            do {
                msg = input.nextLine();
                System.out.println("Recebido: " + msg);
                
            } while (! msg.equalsIgnoreCase("exit"));

            //output.println("Boa noite cliente!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // fase de encerrar a conexão
        try {
            input.close();
            clientSocket.close();
            serverSocket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}