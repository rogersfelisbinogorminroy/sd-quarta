package client;

import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    
    public static void main(String[] args) {
        final String IP = "127.0.0.1";
        final int PORT = 12345;
        Socket socket;
        // Scanner input = null;
        Scanner teclado = null;
        PrintStream output = null;

        // criação do socket e pedido de conexão
        try {
            socket = new Socket(IP, PORT);
        } catch (Exception e) {
            System.out.println("Não foi possivel conectar ao servidor.");
            return;
        }

        // fase de comunicação
        try {
            // input = new Scanner(socket.getInputStream());
            output = new PrintStream(socket.getOutputStream());
            teclado = new Scanner(System.in);

            String msg;
            do {
                System.out.print("Digite a mensagem: ");
                msg = teclado.nextLine();
                output.println(msg);
            } while (! msg.equalsIgnoreCase("exit"));

            // System.out.println("Recebido: " + msg);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // fase de encerramento a conexão
        try {
            output.close();
            socket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
