package chat.server;

import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class AtendeCli extends Thread {
    private Socket cliente;
    private Scanner input = null;
    private PrintStream output = null;
    private ArrayList<AtendeCli> threads;

    public AtendeCli(Socket cliente, ArrayList<AtendeCli> threads) {
        this.cliente = cliente;
        this.threads = threads;
    }

    @Override
    public void run() {
        try {
            input = new Scanner(cliente.getInputStream());
            output = new PrintStream(cliente.getOutputStream());

            String msg;
            do {
                msg = input.nextLine();
                System.out.println("Recebido: " + msg);
                for (AtendeCli atendeCli : threads) {
                    atendeCli.sendMessage(msg);
                }
                
            } while (! msg.equalsIgnoreCase("exit"));

            System.out.println("Encerrada a conexão com " + cliente.getInetAddress().getHostAddress());

            input.close();
            cliente.close();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void sendMessage(String msg) {
        output.println("> " + msg);
    }

}
