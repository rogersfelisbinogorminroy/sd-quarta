package chat.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    static ArrayList<AtendeCli> threads = new ArrayList<>();

    public static void main(String[] args) {
        final int PORT = 12345;
        ServerSocket serverSocket;
        Socket clientSocket = null;
        
        // criar o socket e fazer o bind
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (Exception e) {
            System.out.println("Porta " + PORT + " já está em uso.");
            return;
        }

        // aguardar um pedido de conexão
        try {
            while (true) {
                System.out.println("Aguardando pedido de conexão...");
                clientSocket = serverSocket.accept();
                System.out.println("Conectado com " + clientSocket.getInetAddress().getHostAddress());
                AtendeCli atendeCli = new AtendeCli(clientSocket, threads);
                threads.add(atendeCli);
                atendeCli.start();
            }
        } catch (Exception e) {
            System.out.println("Erro na conexão.");
            System.out.println(e.getMessage());
        }

        // fase de encerrar a conexão
        try {
            serverSocket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}