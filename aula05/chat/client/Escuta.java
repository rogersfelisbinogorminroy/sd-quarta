package chat.client;

import java.net.Socket;
import java.util.Scanner;

public class Escuta extends Thread {
    private Socket socket;
    private Scanner input = null;
    private boolean running;

    public Escuta(Socket socket) {
        this.socket = socket;
        running = true;
    }

    public void parar() {
        running = false;
    }

    @Override
    public void run() {
        try {
            input = new Scanner(socket.getInputStream());

            String msg;
            do {
                msg = input.nextLine();
                System.out.println(msg);
            } while (running);

            input.close();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
